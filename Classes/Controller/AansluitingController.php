<?php
namespace Parousia\Churchpersreg\Controller;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use Parousia\Churchpersreg\Hooks\FluidEmailReal;
use Parousia\Churchpersreg\Domain\Model\Aansluiting;
use Parousia\Churchpersreg\Domain\Repository\PersoonRepository;
use Parousia\Churchpersreg\Domain\Repository\AansluitingRepository;

use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\RawMessage;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Extbase\Property\PropertyMapper;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/***
 *
 * This file is part of the "Aansluiting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * AansluitingController
 */
class AansluitingController extends ActionController
{
    /**
     * @var \Parousia\Churchpersreg\Domain\Repository\AansluitingRepository
     */
    protected $PersoonRepository;
    protected $AansluitingRepository;
	protected static $includedMyJs;
	protected $args=array();
	protected $return='';
	protected $pageId;
	protected $myprofile=0;
	protected $stack=array();
	protected $extensionConfiguration;
	var $userid;
	var $username='';
	var $frontendUser;

	
	public function __construct(
       	AansluitingRepository $aansluitingRepository,
       	PersoonRepository $persoonRepository,
		PropertyMapper $propertyMapper,
    ) 
	{	
		$this->AansluitingRepository = $aansluitingRepository;
		$this->PersoonRepository = $persoonRepository;
		$this->PropertyMapper = $propertyMapper;
	}


    /**
     * Initialize redirects
     */
    public function initializeAction(): void
    {
		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Initialize args '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (isset($this->args['stack']))$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Initialize stack '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if(isset($this->args['return']))$this->return=$this->args['return'];
//		if(isset($this->args['person_uid']))$this->personid=$this->args['person_uid'];
		$pageArguments = $this->request->getAttribute('routing');
		$this->pageId = $pageArguments->getPageId();
		$this->extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchpersreg');
		$this->frontendUser = $this->request->getAttribute('frontend.user');
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Initialize args frontend->user: '.urldecode(http_build_query($this->frontendUser->user,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction personid: '.$this->personid.', userid: '.$this->userid.', user: '.$this->username.', return: '.$this->return.'; args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	}
	
		 /**
     * action aansluitingSend
     *
     * @return void
     */
    public function aansluitingSendAction(): ResponseInterface
    {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'aansluitingSendAction args:'.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (isset($this->args["errorkey"]))$errorkey=$this->args["errorkey"];else $errorkey='';
		$assignedValues = array();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'aansluitingSendAction errorkey:'.$errorkey."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (empty($errorkey))
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'aansluitingSendAction lid:'.$this->frontendUser->user['gedoopt']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$Aansluiting = new Aansluiting();
		}
		else
		{
			// fill assigned values:
			$Aansluiting= $this->PropertyMapper->convert($this->args['aansluiting'],Aansluiting::class); 
			$assignedValues['errorkey']=$errorkey;
		}
		$assignedValues['aansluiting']=$Aansluiting;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'aansluitingSendAction assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();

	}
	

    /**
     * action saveAansluiting
     *
     * @return void
     */
    function saveAansluitingAction(): ResponseInterface
    {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveAansluitingAction args:'.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$errorkey='';
		$typerequest='aansluiting';
		// check if aanvraag al ingediend
		$aansluiting=$this->args['aansluiting'];
		$search=array();
		$search['geboortedatum']=(new \DateTime($aansluiting['geboortedatum']))->format("Y-m-d");
		$search['postcode']=$aansluiting['postcode'];
		$search['huisnummer']=$aansluiting['huisnummer'];
		$aanvragen=$this->AansluitingRepository->findAansluitingSelection($search);
		if (empty($aanvragen))
		{
			$search['typerequest']='aansluiting';
			$persoon=$this->AansluitingRepository->findPersoonSelection($search);
			if (!empty($persoon))$errorkey="persoon.msgalaangesloten";
		}
		else $errorkey="persoon.msgalaanvraag";
		if (empty($errorkey)) 
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveAansluitingAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save aansluitin: '.str_replace('=','="',urldecode(http_build_query($aansluiting,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		    $errorkey=$this->AansluitingRepository->saveAansluiting($aansluiting); 
		}

		if (!empty($errorkey)) 
		{
		//	error_log(date("Y-m-d H:i:s")." - ".$_SERVrER['PHP_SELF'].": ".'saveAansluitingAction gevonden aanvragen:'.urldecode(http_build_query($aanvragen,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$assignedValues=[
				'errorkey' => $errorkey,
				'aansluiting' => $this->args['aansluiting'],
			];

        	return (new ForwardResponse("aansluitingSend"))
              ->withArguments($assignedValues);
		}

	    $assignedValues = [
			'aansluiting' => $this->args['aansluiting'],
   		];
		$aansluiting['naam']=$aansluiting['voornaam'].' '.$aansluiting['achternaam'];
		$aansluitingnaam=$aansluiting['naam'];

		$email=['from'=>new Address($GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username'],$aansluitingnaam),
		'to'=>new Address($this->settings['destinationemail'], $this->settings['destinationname'])];
		if (!empty($this->settings['destinationCC']))
		   $email['cc']= new Address($this->settings['destinationCC'], $this->settings['destinationCCname']);
		$email['reply']= new Address($aansluiting['emailadres'], $aansluitingnaam);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveAansluitingAction email: '.urldecode(http_build_query($email,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		$template='AansluitingAanvraag';
		$this->SendEmailFluid($email,$assignedValues,$template);

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveAansluitingAction confirmation mail person: '.urldecode(http_build_query($aansluiting,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		
		// send confirmation email:
	    $assignedValues = [
		    'namesender' => $this->settings['destinationname'],
			'voornaam' => $aansluiting['voornaam'],
			'naam' => $aansluiting['naam'],
			'typerequest' => 'aansluiting',
   		];

		$email=['from'=>new Address($GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username'],$this->settings['destinationname']),
		'reply'=>new Address($this->settings['destinationemail'], $this->settings['destinationname']),
		'to'=>new Address($aansluiting['emailadres'],$aansluiting['naam'])];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveAansluitingAction confirmation mail voornaam: '.$aansluiting['voornaam'].'; assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$this->SendEmailFluid($email,$assignedValues,'AanvraagBevestiging');

		// return:
	//	if ($this->settings['typerequest']=="lidmaatschap")$assignedValues['errorkey']="persoon.msgthankyourlid";
	//	if ($this->settings['typerequest']=="doop")$assignedValues['errorkey']="persoon.msgthankyourdoop";
		$assignedValues['errorkey']="persoon.msgthankyourequest";
		$assignedValues['typerequest']='aansluiting';

        return (new ForwardResponse("aansluitingConfirmation"));



		//header('Location: '.GeneralUtility::locationHeaderUrl('index.php'));
		//exit;
    }

    /**
     * action aansluitingConfirmation
     *
     * @return void
     */
    function aansluitingConfirmationAction(): ResponseInterface
    {
		return $this->htmlResponse();
	}


	//
	//	Send email fluid to user 
	//

	function SendEmailFluid($email,$assignedValues,$templatename){
		// Prepare and send the message
		$mail= new FluidEmailReal();
		$mail
		   // Give the message a subject
		   ->from($email['from'])
		   // Give it a body
		   ->format('html')
		   ->to($email['to'])
		   ->setTemplate($templatename)
		   ->assignMultiple($assignedValues);
		if (!empty($email['cc']))
		   $mail->cc($email['cc']);
		  if (!empty($email['reply']))
  		   	$mail->replyTo($email['reply']);

		$mail->send();
	}

	
	function rteSafe($strText) 
	{
		//returns safe code for preloading in the RTE
		$tmpString = $strText;
		
		//convert all types of single quotes
		$tmpString = str_replace(chr(145), chr(39), $tmpString);
		$tmpString = str_replace(chr(146), chr(39), $tmpString);
		$tmpString = str_replace("'", "&#39;", $tmpString);
		
		//convert all types of double quotes
		$tmpString = str_replace(chr(147), chr(34), $tmpString);
		$tmpString = str_replace(chr(148), chr(34), $tmpString);
	//	$tmpString = str_replace("\"", "\"", $tmpString);
		
		//replace carriage returns & line feeds
		$tmpString = str_replace(chr(10), " ", $tmpString);
		$tmpString = str_replace(chr(13), " ", $tmpString);
		
		return $tmpString;
	}

	 /**
     * Returns a random string needed to generate a fileName for the queue.
     *
     * @param int $count
     *
     * @return string
     */
    protected function getRandomString(int $count): string
    {
        // This string MUST stay FS safe, avoid special chars
        $base = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-';
        $ret = '';
        $strlen = strlen($base);
        for ($i = 0; $i < $count; ++$i) {
            $ret .= $base[((int)random_int(0, $strlen - 1))];
        }
        return $ret;
    }
	
}
