<?php
namespace Parousia\Churchpersreg\Hooks;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;

/*
* class for ajax to get photo of person uid
*/

class toonfoto 
{

	/**
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
	 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{
		$response = GeneralUtility::makeInstance(Response::class);
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'toonfoto CookieParams '.urldecode(http_build_query($request->getCookieParams(),NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/debug.txt');
		if (!isset($request->getCookieParams()['fe_typo_user']) or empty($request->getCookieParams()['fe_typo_user']))exit();
			
		$aParms=$request->getQueryParams();
		if(isset($aParms))
		{
			$Pid='';
			if (isset($aParms["Pid"]))$Pid=trim($aParms['Pid']);
			if (!is_numeric($Pid))exit();
			$size="large";
			if (isset($aParms["size"]))$size=trim($aParms["size"]);
			$statement="SELECT ".(($size=='small')?'thumbnail as foto':'foto').",mimetypefoto,photoWidth,photoHeight FROM persoon WHERE uid='".$Pid."'";
			$localConf = include str_replace("/public","",$_SERVER['DOCUMENT_ROOT']).'/config/system/settings.php';
			$db = new \mysqli($localConf['DB']['Connections']['Default']['host'], $localConf['DB']['Connections']['Default']['user'], $localConf['DB']['Connections']['Default']['password'], $localConf['DB']['Connections']['Default']['dbname']);
			if ($db->connect_errno) die ("Connect failed: %s\n" . $db->connect_error);
			$result=$db->query($statement);
			$obj=$result->fetch_object();
			ob_start();
			header("Content-Type: ".$obj->mimetypefoto);
			echo $obj->foto;
			ob_end_flush(); 
		}
		return $response;
	}
}
?>
