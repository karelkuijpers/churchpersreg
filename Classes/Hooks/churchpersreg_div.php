<?php
namespace Parousia\Churchpersreg\Hooks;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Information\Typo3Version;
/***************************************************************
*  Copyright notice
*
*  (c) 1999-2008 Karel Kuiojpers (karelkuijpers@gmail.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Contains the reknown class "church_div" with general purpose functions
 *
 * $Id: class.church_div.php  $
 * XHTML compliant
 * Usage counts are based on search 22/2 2003 through whole source including tslib/
 *
 * @author	Karel Kuijpers <karelkuijpers@gmail.com>
 */
/**
/**
 * The legendary "churchpersreg_div" class - Miscellaneous functions for churchregpers purpose.
 * Most of the functions does not relate specifically to TYPO3
 * You are encouraged to use this library in your own scripts!
 *
 * USE:
 * The class is intended to be used without creating an instance of it.
 * So: Don't instantiate - call functions with "church_div::" prefixed the static function name.
 * So use church_div::[method-name] to refer to the functions, eg. 'church_div::milliseconds()'
 *
 * @author	Karel Kuijpers <karelkuijpers@gmail.com>
 * @package TYPO3
 * @subpackage churchadmin
 */
class churchpersreg_div {
	const ADMINFUNC = 'admin';   // name of functionality for administrative rights
	const ADMINGROUP = 'Administrators'; // name of usergoup of administrators
	
	public static $verwredenen= ["0"=>"onbekend","1"=>"verhuisd","2"=>"overleden","5"=>"overgestapt naar andere gemeente","3"=>"onvrede","6"=>"geloof kwijtgeraakt","7"=>"afgehaakt"];


	static function HeeftPermissie($permissie_)
	{
		// ga na of aan de gevraagde $permissie_ in permissies is voldaan.
		$permissieNodigArray_ = explode(",",strtolower($permissie_));
//		if (is_array($permissieNodigArray_))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'permissieNodigArray_ : '.http_build_query($permissieNodigArray_,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		$gevonden_=false;
		$permissie_array_=$GLOBALS['TSFE']->fe_user->getKey('user','permissie');
//		if (is_array($permissie_array_))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'permissie_array_ : '.http_build_query($permissie_array_,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
//		else error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'geen permissies voor  : '.$GLOBALS['TSFE']->fe_user->username."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		if (is_array($permissie_array_) and !empty($permissie_array_) )
		{
			if (in_array(self::ADMINFUNC,$permissie_array_))
				$gevonden_=true;
			else
			{
				foreach ($permissieNodigArray_ as $permissienodig)
				{
					if (in_array($permissienodig,$permissie_array_) )
						{$gevonden_=true;break;}
				}
			}
		}
		return $gevonden_;
	}
		
	static function getImageFile($file){
		$takeFile = fopen($file, "r");
		$file = fread($takeFile, filesize($file));
		fclose($takeFile);
		return $file;
	}
	// make connection with database via mysqli
	static function connectdb(\mysqli &$db = NULL){
		mb_internal_encoding("UTF-8");
		$Typo3Version=GeneralUtility::makeInstance(Typo3Version::class)->getMajorVersion();
		if ($Typo3Version<12)
			$localConf = include $_SERVER['DOCUMENT_ROOT'].'/typo3conf/LocalConfiguration.php';
		else 
			$localConf = include str_replace("/public","",$_SERVER['DOCUMENT_ROOT']).'/config/system/settings.php';
		$db = new \mysqli($localConf['DB']['Connections']['Default']['host'], $localConf['DB']['Connections']['Default']['user'], $localConf['DB']['Connections']['Default']['password'], $localConf['DB']['Connections']['Default']['dbname']) or die("Unable to connect to SQL server");;
		if ($db->connect_errno) die ("Connect failed: %s\n" . $db->connect_error);
		$db->set_charset("utf8");
		$db->query("set session sql_mode = ''");
		$db->query("SET group_concat_max_len=50000;");
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SET @password  : '.$GLOBALS['TYPO3_CONF_VARS']['PARCHURCH']['Connections']['parsl']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		$db->query('SET @password="'.$GLOBALS['TYPO3_CONF_VARS']['PARCHURCH']['Connections']['parsl'].'"');		

	}

	/*
	static function to resize an image and maintain aspect ratio. 
	It will resize jpeg, gif or png and could easily be modified to add bmp. 
	The name field is the destination of the file minus the file extension:
	*/
    static function resizeImage($uploadedfile,&$newWidth,&$newHeight,&$resizedimg,&$thumbimg,&$error)
    {
        // Capture the original size of the uploaded image
        if(!$info=getimagesize($uploadedfile))
		{	$error="Kan formaat van dit type foto niet vaststellen; alleen formaat jpg, gif, bmp en png toegestaan<br>";
			return false;
		}
        switch ($info['mime'])
        {
            case 'image/jpeg':
                $src = imagecreatefromjpeg($uploadedfile);
                break;
            case 'image/gif':
                $src = imagecreatefromgif($uploadedfile);
                break;
            case 'image/png':
                $src = imagecreatefrompng($uploadedfile);
                break;
			case 'image/bmp':
				$src = imagecreatefrombmp($uploadedfile);
				break;
            default:
				{$error="Alleen formaat jpg, gif, bmp en png toegestaan<br>";
	                return false;}
        }
		//		TYPO3\CMS\Core\Utility\GeneralUtility::devLog('createImage foto ingelezen: '.$info['mime'], 'churchadmin');
		if (!$src){$error='kon foto niet lezen';return false;}
		$aspect_ratio = ($info[0]/$info[1]);
		if($aspect_ratio < ($newWidth/$newHeight))
		{
			$newWidth = $newHeight*$aspect_ratio;
		} elseif($aspect_ratio > ($newWidth/$newHeight)) {
			$newHeight = $newWidth/$aspect_ratio;
		}
		$quality=50;

        $tmp=imagecreatetruecolor($newWidth,$newHeight);
       
        // this line actually does the image resizing, copying from the original
        // image into the $tmp image
        if (imagecopyresampled($tmp,$src,0,0,0,0,$newWidth,$newHeight,$info[0], $info[1]))
		{
	        $filename='resize'.rand().".jpg";
       	    imagejpeg($tmp,$filename,$quality); //100 is the quality settings, values range from 0-100.
			$resizedimg = self::getImageFile($filename);
			unlink($filename);
			// make thumbnail:
			$thumbHeight=100;
			$thumbWidth=$newWidth*(100/$newHeight);
	        $thbtmp=imagecreatetruecolor($thumbWidth,$thumbHeight);
//	        if (imagecopyresampled($thbtmp,$tmp,0,0,0,0,$thumbWidth,$thumbHeight,$newWidth, $newHeight))
	        if (imagecopyresampled($thbtmp,$src,0,0,0,0,$thumbWidth,$thumbHeight,$info[0], $info[1]))
	        {	$filename='resize'.rand().".jpg";
	       	    imagejpeg($thbtmp,$filename); //100 is the quality settings, values range from 0-100.
				$thumbimg = self::getImageFile($filename);
			}
			unlink($filename);
			
		}
		else{$error="Kan grootte foto niet aanpassen<br>"; return false;}
           
    	imagedestroy($src);
    	imagedestroy($thbtmp);
        imagedestroy($tmp); // NOTE: PHP will clean up the temp file it created when the request
	        // has completed.

        return true;
    }
	/**
	 * Method Trace to log to a file
	 */
	public static function Trace($strTekst, $errorfile = "Trace.log")
	{
		$extpath='typo3temp/churchadmin/log/';
		if (!file_exists($extpath))mkdir($extpath, 0777,true);
		$filename=$extpath.$errorfile;
		$handle = fopen($filename, "a");
		fclose($handle);	
		date_default_timezone_set("UTC") ;
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".$strTekst."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/'.$filename);
	}

	
	/**
	 * Method ExportToXlsx: export rows to file
	 *
	 * @param	aExport		$array to be exported to xlsx
	 * 
	 * @returns	exportfilename with exported rows
	 */
	public static function ExportToXlsx($aExport,$exportnaam,$sheetname= NULL,$header = NULL,$username = NULL,$styles = NULL, $col_options=NULL){
		$extpath='typo3temp/churchadmin/';
		$extpathabs=$_SERVER['DOCUMENT_ROOT'].'/'.$extpath;
		
		if (empty($username))$username="system";
		$name=str_replace(" ","",$username);
		if (empty($exportnaam))$exportnaam='export';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ExportToXlsx extpath: '.$extpath.'; username:'.$username.'; sheetname:'.$sheetname.'; exportnaam:'.$exportnaam."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		if (!file_exists($extpathabs."export"))mkdir($extpathabs."export", 0777,true);
		$usermap=$extpath."export/".$username;
		$usermapabs=$extpathabs."export/".$username;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ExportToXlsx usermap: '.$usermap."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/mynews/debug.log');
		
		// check if map exists:
		if (!file_exists($usermapabs))mkdir($usermapabs, 0777);
		else // clear map 
			foreach (glob($usermapabs."/*") as $filename)unlink($filename); 								  

		$now=new \DateTime();
		$exportfilenaam=$usermap.'/'.$exportnaam.$name.'_'.$now->format('Y-m-d').".xlsx";;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ExportToXlsx exportfilenaam: '.$exportfilenaam."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/mynews/debug.log');
		// remove old files of user first:
		if ($handle = opendir($usermapabs)) 
		{	while (false !== ($file = readdir($handle))) 
			{	if (is_file($file) && strstr($file,$name)!==false) unlink($usermapabs."/".$file);} 
			closedir($handle); 
		} 
		$eerstekeer=true;
		if (empty($sheetname)) $sheetname=$exportnaam;
		$writer = new \XLSXWriter();     //new writer
		$data=array();
		if (empty($header))
		{
			if (isset($aExport[0]) and is_array($aExport[0]))$header=array_keys($aExport[0]); 
			else $header=array();
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ExportToXlsx header: '.urldecode(http_build_query($header,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		//if (!empty($col_options))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ExportToXlsx col_options: '.GeneralUtility::array2xml($col_options)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		$header=array_fill_keys($header,'string');
		$writer->writeSheetHeader($sheetname,$header,$col_options);
//		$styles=array(['valign'=>'top','width'=>'20'],['valign'=>'top','width'=>'20','wrapText'=>'1']);
//		$styles=array(['valign'=>'top'],['valign'=>'top','wrap_text'=>true]);
		if (is_array($aExport))
		{
			foreach ($aExport as $Export)
			{	
				$writer->writeSheetRow($sheetname,array_values($Export),$styles);
			//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ExportToXlsx row: '.urldecode(http_build_query(array_values($Export),NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
			}
		}
		$writer->setAuthor($username);
		$writer->writeToFile($_SERVER['DOCUMENT_ROOT'].'/'.$exportfilenaam);
		return $exportfilenaam;
	}

}

	


?>