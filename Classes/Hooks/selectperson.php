<?php
namespace Parousia\Churchpersreg\Hooks;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;

/*
* class for ajax to get personid and name for searchname and required role (partner, farther, mother) with person idperson
*/

class selectperson 
{

	/**
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
	 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{

		$response = GeneralUtility::makeInstance(Response::class);

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin getperson: ".$_SERVER['DOCUMENT_ROOT']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin getperson na post:".count($POST)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		churchpersreg_div::connectdb($db);
		$zoeknaam='';
		$pid=0;
		$rol='';
		$aParms=$request->getParsedBody(); 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": getperson parsbody:$aParms"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (isset($aParms["naam"]))$zoeknaam = $aParms["naam"];
		if (isset($aParms["idperson"]))$pid=$aParms["idperson"];
		if (isset($aParms["rol"]))$rol=$aParms["rol"];
		
		// bepalen van person-id/feuseruid bij opgegeven zoeknaam (voornaam + achternaam, b.v. partner, vader, organiser
		// person-id moet ongelijk eigen $pid zijn, indien ingevuld.
		// bij fout met lege id en foutmelding in self::$ErrMsg)
		$naamstr='trim(concat(roepnaam,if(tussenvoegsel != "",concat(" ",tussenvoegsel),"")," ",achternaam))';
		$a_naam=explode(" ",strtolower($zoeknaam)); // splits in voornaam, achternaam, etc.
		$aZoek=preg_split("/[\s,]+/",$zoeknaam);
		$where='';
		foreach ($aZoek as $zoek){
			if (!empty($zoek))$where.=(empty($where)?'':" and ")."$naamstr like '%".$db->real_escape_string($zoek)."%'";
		} 
		$where.=(empty($where)?'':" and ").'p.deleted=0 '; 
		if (!empty($pid)){$where.=(empty($where)?'':" and ")."p.`uid` != '".$pid."'";}
		if ($rol=="vader"){$where.=(empty($where)?'':" and ")."`geslacht`='man' ".(empty($pid)?'':" and `id_vader` != '".$pid."'");}
		if ($rol=="moeder"){$where.=(empty($where)?'':" and ")."`geslacht`='vrouw' ".(empty($pid)?'':" and `id_moeder` != '".$pid."'");}
		if ($rol=="partner"){$where.=(empty($where)?'':" and ")."(`id_partner` = '' ".(empty($pid)?'':" or `id_partner` = '".$pid."'").")";}
		if ($rol=="organiser")$where.= " and p.uid=f.person_id";
//		$query="SELECT $naamstr as naam,p.`uid` as id,AES_DECRYPT(p.emailadres,@password) as emailadres".(($rol=="organiser")?",f.`uid`":"")." from `persoon` p".(($rol=="organiser")?",fe_users f":"")." where $where order by `achternaam` limit 15";
		$query="SELECT $naamstr as naam,".(($rol=="organiser")?"f.`uid`":"p.`uid`")." as id,AES_DECRYPT(p.emailadres,@password) as emailadres from `persoon` p".(($rol=="organiser")?",fe_users f":"")." where $where order by `achternaam` limit 15";
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": getperson query:".$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$result=$db->query($query) or die("Can't perform Query");	
		$rows=array();
		//$i=0;
		while ($row=$result->fetch_array(MYSQLI_ASSOC))
		{
			
			$rows[]=$row;
			//$i++;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'get role result['.$i.'-'.$row['naam'].']: '.json_encode($row['naam'],JSON_HEX_TAG)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		}
		
		if (!empty($rows))
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'get role result: '.http_build_query($rows,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$data= array('status'=>'success','message'=>'','data'=>$rows);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'json getting role:'.json_encode($data,JSON_HEX_TAG)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		} else {
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Error getting role:  '.$sqlerr."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$data= array('status'=>'error','message'=>'Kan '.$rol.' niet vinden','data'=>'');
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'get role data: '.http_build_query($data,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$response->getBody()->write(json_encode($data));
		return $response;
	}
}



 

