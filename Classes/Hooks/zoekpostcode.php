<?php
namespace Parousia\Churchpersreg\Hooks;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;

/*
* class for ajax to get personid and name for searchname and required role (partner, farther, mother) with person idperson
*/

class zoekpostcode 
{

	/**
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
	 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{

		$response = GeneralUtility::makeInstance(Response::class);

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin zoekpostcode: "."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
		$woonplaats="";
		$straat="";
		$postcode='';
		$data=array();
		$httpfile='';
		$fout=false;
		$aParms=$request->getParsedBody();
		if(isset($aParms))
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": zoekpostcode parsbody aParms:".urldecode(http_build_query($aParms,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
			if (isset($aParms["postcode"]))$postcode=strtoupper(trim($aParms['postcode']));else $postcode='';
			$postcode=str_replace(' ', '', $postcode);
			if (isset($aParms['nr']))$huisnr=rtrim($aParms['nr']);else $huisnr="";
			$api='https://api.pdok.nl/bzk/locatieserver/search/v3_1/free?q='.$postcode.'%20'.$huisnr.'&fq=type:(adres)&rows=1&fl=straatnaam,woonplaatsnaam';
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": zoekpostcode api: ".$api."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
			try {
				$httpfile  = file_get_contents($api);
		        $obj  = json_decode($httpfile);
			} catch(Exception $e) {
				$fout=true;
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": zoekpostcode foute aanroep bzk: ".$e.$_SERVER['DOCUMENT_ROOT']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
			} 
			if (!$fout )
			{
			    // false als response onjuist is
			    try {
			        $numFound  = $obj->response->numFound;
			        $arrOutput = $obj->response->docs;
			    } catch (Exception $e) {
					$fout=true;
			    }
			    if (!$fout and $numFound >0) 
				{
					$straat=$arrOutput[0]->straatnaam;
					$woonplaats=$arrOutput[0]->woonplaatsnaam;
					if (strpos(strtolower($woonplaats),'gravenhage')!==false)$woonplaats='Den Haag';
				}
			}				

		}		

		$data= array('status'=>'success','message'=>'','woonplaats' => $woonplaats,'straat'=> $straat);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'json getting role:'.json_encode($data,JSON_HEX_TAG)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
		$response->getBody()->write(json_encode($data));
		return $response;
	}
}


