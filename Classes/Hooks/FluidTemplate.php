<?php
namespace Parousia\Churchpersreg\Hooks;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

 Class FluidTemplate
 {
 
 	/**
	 * Returns the rendered fluid email template
	 * @param string $template
	 * @param array $assign
	 * @param string $ressourcePath
	 * @return string
	 */
	static function render($template, Array $assign = array(), $caller,$extensionname,$partialextension='') {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluid render start template:'.$template."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		$view = GeneralUtility::makeInstance(StandaloneView::class);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate view created'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		if (isset($caller->request)) 
		{
			$view->setRequest($caller->request);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate after setRequest' ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		}
		$rootPath='EXT:'.$extensionname.'/Resources/Private/';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate rootPath: '.$rootPath."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
	    $rootabsPath = GeneralUtility::getFileAbsFileName($rootPath);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate rootabsPath: '.$rootabsPath."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		$layoutPath=$rootabsPath . 'Layouts/';
		$layoutPaths=array($layoutPath);
	    $view->setLayoutRootPaths($layoutPaths);
		$templatePath=$rootabsPath . 'Templates/'.$template;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate templatePath : '.$templatePath."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
	    $view->setTemplatePathAndFilename($templatePath);
		if (!empty($partialextension))
		{
			$partialPath='EXT:'.$partialextension.'/Resources/Private/';
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate rootPath: '.$rootPath."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		    $partialabsPath = GeneralUtility::getFileAbsFileName($partialPath);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate partialabsPath: '.$partialabsPath."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
			$view->setPartialRootPaths(array($partialabsPath.'Partials/'));
		}
		
	    $view->assignMultiple($assign);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate assign : '.urldecode(http_build_query($assign,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
	    try {
			return $view->render();
	    } catch (\Exception $e) {
			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate error : '.$e->getMessage()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
	        return null;
	    }
	}
}