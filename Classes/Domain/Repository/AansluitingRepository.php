<?php

namespace Parousia\Churchpersreg\Domain\Repository;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Connection;


/**
 * Class AansluitingRepository
 *
 * @package Parousia\Churchpersreg\Domain\Repository
 *
 * return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
 */
class AansluitingRepository 
{

	protected $db;
 /**
     * findAansluitingSelection function 
     *
     * param array  $search
     *
     * return nbr found
     */

	public function findAansluitingSelection(array $search = null)
	{
		churchpersreg_div::connectdb($this->db);
		$where='';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findAansluitingSelection search: '.urldecode(http_build_query($search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		foreach ($search as $key => $value)
		{
			if (!empty($where))$where.=' and ';
			$where.=$key.'="'.$value.'"';
		}
		if (!empty($where))$where.=' and ';
		$where.="deleted=0";

		$statement="select count(*) as nbr from aansluiting where ".$where;
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findAansluitingSelection statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$result=$this->db->query($statement) or die("Can't Aansluiting Query");	
		$row=$result->fetch_array(MYSQLI_ASSOC);
		$nbr=$row['nbr'];
		return $nbr;
    }

		
 /**
     * findPersoonSelection function 
     *
     * param array  $search
     *
     * return nbr found
     */

	public function findPersoonSelection(array $search = null)
	{
		churchpersreg_div::connectdb($this->db);
		$zoekargument=$this->ComposeSearch($search);
		$statement = 'SELECT count(*) as nbr'.
		' FROM persoon as tp,adres as ta'.
		' where '.$zoekargument;

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findPersoonSelection statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$result=$this->db->query($statement) or die("Can't perAansluiting Query");	
		$row=$result->fetch_array(MYSQLI_ASSOC);
		$nbr=$row['nbr'];
		return $nbr;
    }

		/**
	 * Method 'ComposeSearch' for the 'churchpersreg' extension.
 	*
	 * param searchfield: keyword required persons ({$Zoekveld,$Zoekveld_1,$Zoekveld_2,$Zoekveld_3,$Zoekveld_4,$Zoekveld_5,$Zoekveld_6})
	 * returns composed sql search parameter
	 */
	function ComposeSearch(array $search = null)
	{	
	//	Samenstellen van zoekargument
	//			$this->ErrMsg.="Compose zoekveld 1:".$Zoekveld[1].",2:".$Zoekveld[2].",3:".$Zoekveld[3].",4:".$Zoekveld[4].",5:".$Zoekveld[5].",6:".$Zoekveld[6].",7:".$Zoekveld[7];
		$zoekargument="";
		$connection=\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getConnectionForTable('string');
	/*	if (!empty($Zoekveld[7])){
			$zoekargument .=  "(tp.uid = '" . addslashes($Zoekveld[7]) ."') AND ";}		*/
		if ($search['geboortedatum']<>""){
			$zoekargument .=  'tp.geboortedatum = "'.$search['geboortedatum'].'" AND ';}
		if ($search['postcode']<>""){ 
			$zoekargument .=  "ta.postcode LIKE '%" . addslashes($search['postcode']) ."%' AND ";}
		if ($search['huisnummer']<>""){ 
			$search['huisnummer']=trim($search['huisnummer']);
			$zoekargument .=  "ta.huisnummer LIKE '%" . addslashes($search['huisnummer']) ."%' AND ";
		}

		$zoekargument.=' AES_DECRYPT(tp.id_adres,@password)=ta.uid and tp.deleted=0 and ta.deleted=0';
//		$this->ErrMsg.=",br>Zoekargument:".$zoekargument;
		return $zoekargument;
	}
	
	
	/**
	 * Method 'saveUsergroup' for the 'churchauthreg' extension.
 	*
	 */
	
	public function saveAansluiting(array $aansluiting): string
	{
		if (array_key_exists('geboortedatum',$aansluiting))
		{	
			$gbdat = new \DateTime($aansluiting['geboortedatum']);
			if (!is_object($gbdat))
			{
				//$this->ErrMsg="geboortedatum ".$aansluiting['geboortedatum']." is een ongeldige datum";
				return 'aansluiting.invalidbirthdate';
			}

			if ($gbdat > new \DateTime())
			{
				//$this->ErrMsg="geboortedatum kan niet in de toekomst liggen";
				return 'aansluiting.invalidbirthdate';
			}
			if ($gbdat < new \DateTime("- 130 years"))
			{
				//$this->ErrMsg="geboortedatum - ".$person['geboortedatum']." - te ver in het verleden";
				return 'aansluiting.invalidbirthdate';
			}
			$aansluiting['geboortedatum']=$gbdat->format("Y-m-d");
		}
		$aansluiting['voornaam']=ucfirst($aansluiting['voornaam']);
		$aansluiting['achternaam']=ucfirst($aansluiting['achternaam']);
		$connectionfortable = GeneralUtility::makeInstance(ConnectionPool::class)
    	->getConnectionForTable('aansluiting');			
   		$connectionfortable->insert(
				'aansluiting',
				$aansluiting);				
		// bepaal uid:
//		$uid = $connectionfortable->lastInsertId('aansluiting');
		return '';
	}

	
}