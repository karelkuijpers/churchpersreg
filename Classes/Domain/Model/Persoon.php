<?php
namespace Parousia\Churchpersreg\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***
 *
 * This file is part of the "Sermons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * A speaker is a person who does the preaching
 */
class Persoon extends AbstractEntity
{
    /**
     * roepnaam
     *
     * @var string
     */
    protected $roepnaam = '';

     /**
     * voornamen
     *
     * @var string
     */
    protected $voornamen = '';

    /**
     * tussenvoegsel
     *
     * @var string
     */
    protected $tussenvoegsel = '';

   /**
     * achternaam
     *
     * @var string
     */
    protected $achternaam = '';

   /**
     * lastname
     *
     * @var string
     */
    protected $lastname = '';

    /**
     * burgerlijke_staat
     *
     * @var string
     */
    protected $burgerlijke_staat = '';

    /**
     * trouwdatum
     *
     * @var string
     */
    protected $trouwdatum = NULL;

    /**
     * naam
     *
     * @var string
     */
    protected $naam = '';

    /**
     * mobieltelnr
     *
     * @var string
     */
    protected $mobieltelnr = '';

    /**
     * mobiel_op_website
     *
     * @var bool
     */
    protected $mobiel_op_website= 1;


    /**
     * emailadres
     *
     * @var string
     */
    protected $emailadres = '';

    /**
     * email_op_website
     *
     * @var bool
     */
    protected $email_op_website= 1;

    /**
     * beroep
     *
     * @var string
     */
    protected $beroep = '';

    /**
     * geslacht
     *
     * @var string
     */
    protected $geslacht = '';

    /**
     * geboortedatum
     *
     * @var string
     */
    protected $geboortedatum = NULL;

    /**
     * geboortedatum_op_intranet
     *
     * @var int
     */
    protected $geboortedatum_op_intranet= 1;

    /**
     * geboorteplaats
     *
     * @var string
     */
    protected $geboorteplaats = '';

    /**
     * datumoverlijden
     *
     * @var string
     */
    protected $datumoverlijden = NULL;

    /**
     * bezoeker
     *
     * @var string
     */
    protected $bezoeker = '';

    /**
     * invoerdatum
     *
     * @var string
     */
    protected $invoerdatum = '';

    /**
     * gedoopt
     *
     * @var string
     */
    protected $gedoopt= '';

    /**
     * doopdatum
     *
     * @var string
     */
    protected $doopdatum = NULL;

    /**
     * doopplaats
     *
     * @var string
     */
    protected $doopplaats= '';

    /**
     * doopgemeente
     *
     * @var string
     */
    protected $doopgemeente = '';

    /**
     * lid
     *
     * @var string
     */
    protected $lid = '';

    /**
     * datum_lidmaatschap
     *
     * @var string
     */
    protected $datum_lidmaatschap = NULL;

    /**
     * cleanteam
     *
     * @var string
     */
    protected $cleanteam = '';

    /**
     * taken
     *
     * @var string
     */
    protected $taken = '';

    /**
     * steller
     *
     * @var int
     */
    protected $steller = 0;

    /**
     * stellernaam
     *
     * @var string
     */
    protected $stellernaam = '';

    /**
     * datum_wijziging
     *
     * @var \DateTime
     */
    protected $datum_wijziging = NULL;

    /**
     * verwijderreden
     *
     * @var int
     */
    protected $verwijderreden = 0;

    /**
     * huisgenoten
     *
     * @var string
     */
    protected $huisgenoten = '';

    /**
     * person_uid
     *
     * @var int
     */
    protected $person_uid = 0;

    /**
     * id_partner
     *
     * @var int
     */
    protected $id_partner = 0;

    /**
     * partnernaam
     *
     * @var string
     */
    protected $partnernaam ='';

    /**
     * children
     *
     * @var string
     */
    protected $children ='';

    /**
     * id_vader
     *
     * @var int
     */
    protected $id_vader = 0;

    /**
     * vadernaam
     *
     * @var string
     */
    protected $vadernaam ='';

    /**
     * id_moeder
     *
     * @var int
     */
    protected $id_moeder = 0;

    /**
     * moedernaam
     *
     * @var string
     */
    protected $moedernaam ='';

/** Adres
*/

    /**
     * id_adres
     *
     * @var int
     */
    protected $id_adres = 0;

    /**
     * straatnaam
     *
     * @var string
     */
    protected $straatnaam = '';

    /**
     * huisnummer
     *
     * @var string
     */

    protected $huisnummer = '';

    /**
     * postcode
     *
     * @var string
     */

    protected $postcode = '';

    /**
     * woonplaats
     *
     * @var string
     */
    protected $woonplaats = '';

    /**
     * land
     *
     * @var string
     */
    protected $land = '';

    /**
     * telefoonnr_vast
     *
     * @var string
     */
    protected $telefoonnr_vast = '';

    /**
     * tel_op_website
     *
     * @var string
     */
    protected $tel_op_website = '';

    /**
     * adres_op_website
     *
     * @var string
     */
    protected $adres_op_website = '';

    /**
     * deleted
     *
     * @var int
     */
    protected $deleted = 0;

    /**
     * partnerdeleted
     *
     * @var int
     */
    protected $partnerdeleted = 0;

    /**
     * vaderdeleted
     *
     * @var int
     */
    protected $vaderdeleted = 0;

    /**
     * moederdeleted
     *
     * @var int
     */
    protected $moederdeleted = 0;

    /**
     * foto
     *
     * @var string
     */
    protected $foto = null;

/**
* org values
*/

    /**
     * postcodeorg
     *
     * @var string
     */
    protected $postcodeorg = '';

    /**
     * huisnummerorg
     *
     * @var string
     */
    protected $huisnummerorg = '';

    /**
     * woonplaatsorg
     *
     * @var string
     */
    protected $woonplaatsorg = '';

    /**
     * straatnaamorg
     *
     * @var string
     */
    protected $straatnaamorg = '';

    /**
     * telefoonorg
     *
     * @var string
     */
    protected $telefoonorg = '';

    /**
     * partnerorg
     *
     * @var string
     */
    protected $partnerorg = '';

    /**
     * burgerlijke_staatorg
     *
     * @var string
     */
    protected $burgerlijke_staatorg = '';



/**
* Getters and Setters
*/

    /**
     * Returns the roepnaam
     *
     * @return string $roepnaam
     */
    public function getRoepnaam(): string
    {
        return $this->roepnaam;
    }
    /**
     * Sets the roepnaam
     *
     * @param string $roepnaam
     */
    public function setRoepnaam($roepnaam): void
    {
        $this->roepnaam = $roepnaam;
    }

    /**
     * Returns the voornamen
     *
     * @return string $voornamen
     */
    public function getVoornamen(): string
    {
        return $this->voornamen;
    }
    /**
     * Sets the voornamen
     *
     * @param string $voornamen
     */
    public function setVoornamen($voornamen): void
    {
        $this->voornamen = $voornamen;
    }

   /**
     * Returns the achternaam
     *
     * @return string $achternaam
     */
    public function getAchternaam(): string
    {
        return $this->achternaam;
    }

    /**
     * Sets the achternaam
     *
     * @param string $achternaam
     */
    public function setAchternaam($achternaam): void
    {
        $this->achternaam = $achternaam;
    }


   /**
     * Returns the lastname
     *
     * @return string $lastname
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * Sets the lastname
     *
     * @param string $lastname
     */
    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * Returns the naam
     *
     * @return string $naam
     */
    public function getNaam(): string
    {
        return $this->naam;
    }
    /**
     * Sets the naam
     *
     * @param string $naam
     */
    public function setNaam($naam): void
    {
        $this->naam = $naam;
    }

    /**
     * Returns the tussenvoegsel
     *
     * @return string $tussenvoegsel
     */
    public function getTussenvoegsel(): string
    {
        return $this->tussenvoegsel;
    }

    /**
     * Sets the tussenvoegsel
     *
     * @param string $tussenvoegsel
     */
    public function setTussenvoegsel($tussenvoegsel): void
    {
        $this->tussenvoegsel = $tussenvoegsel;
    }

    /**
     * Returns the burgerlijke_staat
     *
     * @return string|null $burgerlijke_staat
     */
    public function getBurgerlijkeStaat(): ?string
    {
        return $this->burgerlijke_staat;
    }

    /**
     * Sets the burgerlijke_staat
     *
     * @param string $burgerlijke_staat
     */
    public function setBurgerlijkeStaat($burgerlijke_staat): void
    {
        $this->burgerlijke_staat = $burgerlijke_staat;
    }


    /**
     * Returns the emailadres
     *
     * @return string $emailadres
     */
    public function getEmailadres(): string
    {
        return $this->emailadres;
    }

    /**
     * Sets the emailadres
     *
     * @param string $emailadres
     */
    public function setEmailadres($emailadres): void
    {
        $this->emailadres = $emailadres;
    }

    /**
     * Sets the email_op_website
     *
     * @param bool $email_op_website
     */
    public function setEmailOpWebsite($email_op_website): void
    {
        $this->email_op_website = $email_op_website;
    }

    /**
     * Get email_op_website
     *
     * @return bool
     */
    public function getEmailOpWebsite(): bool
    {
        return $this->email_op_website;
    }

    /**
     * Returns the mobieltelnr
     *
     * @return string $mobieltelnr
     */
    public function getMobieltelnr(): string
    {
        return $this->mobieltelnr;
    }

    /**
     * Sets the mobieltelnr
     *
     * @param string $mobieltelnr
     */
    public function setMobieltelnr($mobieltelnr): void
    {
        $this->mobieltelnr = $mobieltelnr;
    }

    /**
     * Sets the mobiel_op_website
     *
     * @param bool $mobiel_op_website
     */
    public function setMobielOpWebsite($mobiel_op_website): void
    {
        $this->mobiel_op_website = $mobiel_op_website;
    }

    /**
     * Get mobiel_op_website
     *
     * @return bool
     */
    public function getMobielOpWebsite(): bool
    {
        return $this->mobiel_op_website;
    }

    /**
     * Returns the beroep
     *
     * @return string $beroep
     */
    public function getBeroep(): string
    {
        return $this->beroep;
    }

    /**
     * Sets the beroep
     *
     * @param string $beroep
     */
    public function setBeroep($beroep): void
    {
        $this->beroep = $beroep;
    }

    /**
     * Returns the geslacht
     *
     * @return string|nulll $geslacht
     */
    public function getGeslacht(): ?string
    {
        return $this->geslacht;
    }

    /**
     * Sets the geslacht
     *
     * @param string $geslacht
     */
    public function setGeslacht($geslacht): void
    {
        $this->geslacht = $geslacht;
    }

   /**
     * Returns the geboortedatum
     *
     * @return string|null $geboortedatum
    */
    public function getGeboortedatum(): ?string
    {
        return $this->geboortedatum;
    }
	/**
     * Sets the geboortedatum
     *
     * @param \DateTime $geboortedatum
     * @return void
    */
    public function setGeboortedatum($geboortedatum): void
    {
        $this->geboortedatum = $geboortedatum;
    }

    /**
     * Sets the geboortedatum_op_intranet
     *
     * @param int $geboortedatum_op_intranet
     */
    public function setGeboortedatumOpIntranet($geboortedatum_op_intranet): void
    {
        $this->geboortedatum_op_intranet = $geboortedatum_op_intranet;
    }

    /**
     * Get geboortedatum_op_intranet
     *
     * @return int
     */
    public function getGeboortedatumOpIntranet(): int
    {
        return $this->geboortedatum_op_intranet;
    }

    /**
     * Returns the geboorteplaats
     *
     * @return string $geboorteplaats
     */
    public function getGeboorteplaats(): string
    {
        return $this->geboorteplaats;
    }

    /**
     * Sets the geboorteplaats
     *
     * @param string $geboorteplaats
     */
    public function setGeboorteplaats($geboorteplaats): void
    {
        $this->geboorteplaats = $geboorteplaats;
    }

   /**
     * Returns the datumoverlijden
     *
     * @return string|NULL $datumoverlijden
    */
    public function getDatumoverlijden(): ?string
    {
        return $this->datumoverlijden;
    }
	/**
     * Sets the datumoverlijden
     *
     * @param \DateTime $datumoverlijden
     * @return void
    */
    public function setDatumoverlijden($datumoverlijden): void
    {
        $this->datumoverlijden = $datumoverlijden;
    }

   /**
     * Returns the trouwdatum
     *
     * @return string|null $trouwdatum
    */
    public function getTrouwdatum(): ?string
    {
        return $this->trouwdatum;
    }
	/**
     * Sets the trouwdatum
     *
     * @param \DateTime $trouwdatum
     * @return void
    */
    public function setTrouwdatum($trouwdatum): void
    {
        $this->trouwdatum = $trouwdatum;
    }

    /**
     * Returns the bezoeker
     *
     * @return string $bezoeker
     */
    public function getBezoeker(): string
    {
        return $this->bezoeker;
    }

    /**
     * Sets the bezoeker
     *
     * @param string $bezoeker
     */
    public function setBezoeker($bezoeker): void
    {
        $this->bezoeker = $bezoeker;
    }

   /**
     * Returns the invoerdatum
     *
     * @return string|NULL $invoerdatum
    */
    public function getInvoerdatum(): ?string
    {
        return $this->invoerdatum;
    }
	/**
     * Sets the invoerdatum
     *
     * @param \DateTime $invoerdatum
     * @return void
    */
    public function setInvoerdatum($invoerdatum): void
    {
        $this->invoerdatum = $invoerdatum;
    }

    /**
     * Returns the gedoopt
     *
     * @return string $gedoopt
     */
    public function getGedoopt(): string
    {
        return $this->gedoopt;
    }

    /**
     * Sets the gedoopt
     *
     * @param string $gedoopt
     */
    public function setGedoopt($gedoopt): void
    {
        $this->gedoopt = $gedoopt;
    }

   /**
     * Returns the doopdatum
     *
     * @return string|NULL $doopdatum
    */
    public function getDoopdatum(): ?string
    {
        return $this->doopdatum;
    }
	/**
     * Sets the doopdatum
     *
     * @param \DateTime $doopdatum
     * @return void
    */
    public function setDoopdatum($doopdatum): void
    {
        $this->doopdatum = $doopdatum;
    }

    /**
     * Returns the doopplaats
     *
     * @return string $doopplaats
     */
    public function getDoopplaats(): string
    {
        return $this->doopplaats;
    }

    /**
     * Sets the doopplaats
     *
     * @param string $doopplaats
     */
    public function setDoopplaats($doopplaats): void
    {
        $this->doopplaats = $doopplaats;
    }

    /**
     * Returns the doopgemeente
     *
     * @return string $doopgemeente
     */
    public function getDoopgemeente(): string
    {
        return $this->doopgemeente;
    }

    /**
     * Sets the doopgemeente
     *
     * @param string $doopgemeente
     */
    public function setDoopgemeente($doopgemeente): void
    {
        $this->doopgemeente = $doopgemeente;
    }

    /**
     * Returns the lid
     *
     * @return string|NULL $lid
     */
    public function getlid(): ?string
    {
        return $this->lid;
    }

    /**
     * Sets the lid
     *
     * @param string $lid
     */
    public function setlid($lid): void
    {
        $this->lid = $lid;
    }

   /**
     * Returns the datum_lidmaatschap
     *
     * @return string|NULL $datum_lidmaatschap
    */
    public function getDatumLidmaatschap(): ?string
    {
        return $this->datum_lidmaatschap;
    }
	/**
     * Sets the datum_lidmaatschap
     *
     * @param \DateTime $datum_lidmaatschap
     * @return void
    */
    public function setDatumLidmaatschap($datum_lidmaatschap): void
    {
        $this->datum_lidmaatschap = $datum_lidmaatschap;
    }

    /**
     * Returns the cleanteam
     *
     * @return string $cleanteam
     */
    public function getCleanteam(): string
    {
        return $this->cleanteam;
    }

    /**
     * Sets the cleanteam
     *
     * @param string $cleanteam
     */
    public function setCleanteam($cleanteam): void
    {
        $this->cleanteam = $cleanteam;
    }

    /**
     * Returns the taken
     *
     * @return string $taken
     */
    public function getTaken(): string
    {
        return $this->taken;
    }

    /**
     * Sets the taken
     *
     * @param string $taken
     */
    public function setTaken($taken): void
    {
        $this->taken = $taken;
    }

    /**
     * Returns the steller
     *
     * @return int $steller
     */
    public function getSteller(): int
    {
        return $this->steller;
    }

    /**
     * Sets the steller
     *
     * @param int $steller
     */
    public function setSteller($steller): void
    {
        $this->steller = $steller;
    }

    /**
     * Returns the stellernaam
     *
     * @return string $stellernaam
     */
    public function getStellernaam(): string
    {
        return $this->stellernaam;
    }

    /**
     * Sets the stellernaam
     *
     * @param string $stellernaam
    */
    public function setStellernaam($stellernaam): void
    {
        $this->stellernaam = $stellernaam;
	}

   /**
     * Returns the datum_wijziging
     *
     * @return string|null $datum_wijziging
    */
    public function getDatumWijziging(): ?string
    {
        return $this->datum_wijziging;
    }
	/**
     * Sets the datum_wijziging
     *
     * @param \DateTime $datum_wijziging
     * @return void
    */
    public function setDatumWijziging($datum_wijziging): void
    {
        $this->datum_wijziging = $datum_wijziging;
    }

    /**
     * Returns the verwijderreden
     *
     * @return int $verwijderreden
     */
    public function getVerwijderreden(): int
    {
        return $this->verwijderreden;
    }

    /**
     * Sets the huisgenoten
     *
     * @param string $huisgenoten
     */
    public function setHuisgenoten($huisgenoten): void
    {
        $this->huisgenoten = $huisgenoten;
    }

    /**
     * Returns the huisgenoten
     *
     * @return string $huisgenoten
     */
    public function getHuisgenoten(): string
    {
        return $this->huisgenoten;
    }

    /**
     * Sets the verwijderreden
     *
     * @param int $verwijderreden
     */
    public function setVerwijderreden($verwijderreden): void
    {
        $this->verwijderreden = $verwijderreden;
    }

    /**
     * Sets the person_uid
     *
     * @param int $person_uid
     */
    public function setPersonUid($person_uid): void
    {
        $this->person_uid = $person_uid;
    }

    /**
     * Returns the person_uid
     *
     * @return int|null $person_uid
     */
    public function getPersonUid(): ?int
    {
        return $this->person_uid;
    }

    /**
     * Sets the id_vader
     *
     * @param int $id_vader
     */
    public function setIdVader($id_vader): void
    {
        $this->id_vader = $id_vader;
    }

    /**
     * Returns the id_vader
     *
     * @return int $id_vader
     */
    public function getIdVader(): int
    {
        return $this->id_vader;
    }

    /**
     * Returns the vadernaam
     *
     * @return string|null $vadernaam
     */
    public function getVadernaam(): ?string
    {
        return $this->vadernaam;
    }

    /**
     * Sets the vadernaam
     *
     * @param string $vadernaam
    */
    public function setVadernaam($vadernaam): void
    {
        $this->vadernaam = $vadernaam;
	}

    /**
     * Returns the id_moeder
     *
     * @return int $id_moeder
     */
    public function getIdMoeder(): int
    {
        return $this->id_moeder;
    }
    /**
     * Sets the id_moeder
     *
     * @param int $id_moeder
     */
    public function setIdMoeder($id_moeder): void
    {
        $this->id_moeder = $id_moeder;
    }

    /**
     * Returns the moedernaam
     *
     * @return string|null $moedernaam
     */
    public function getMoedernaam(): ?string
    {
        return $this->moedernaam;
    }

    /**
     * Sets the moedernaam
     *
     * @param string $moedernaam
    */
    public function setMoedernaam($moedernaam): void
    {
        $this->moedernaam = $moedernaam;
	}

    /**
     * Sets the id_partner
     *
     * @param int $id_partner
     */
    public function setIdPartner($id_partner): void
    {
        $this->id_partner = $id_partner;
    }

    /**
     * Returns the id_partner
     *
     * @return int|null $Id_partner
     */
    public function getidPartner(): ?int
    {
        return $this->id_partner;
    }

    /**
     * Returns the partnernaam
     *
     * @return string|null $partnernaam
     */
    public function getPartnernaam(): ?string
    {
        return $this->partnernaam;
    }

    /**
     * Sets the partnernaam
     *
     * @param string $partnernaam
    */
    public function setPartnernaam($partnernaam): void
    {
        $this->partnernaam = $partnernaam;
	}

    /**
     * Returns the children
     *
     * @return string|null $children
     */
    public function getChildren(): ?string
    {
        return $this->children;
    }

    /**
     * Sets the children
     *
     * @param string $children
    */
    public function setChildren($children): void
    {
        $this->children = $children;
	}
     




    /**
     * Sets the id_adres
     *
     * @param int $id_adres
     */
    public function setIdAdres($id_adres): void
    {
        $this->id_adres = $id_adres;
    }

    /**
     * Returns the id_adres
     *
     * @return int $id_adres
     */
    public function getIdAdres(): int
    {
        return $this->id_adres;
    }

    /**
     * Returns the straatnaam
     *
     * @return string $straatnaam
     */
    public function getStraatnaam(): string
    {
        return $this->straatnaam;
    }

    /**
     * Sets the straatnaam
     *
     * @param string $straatnaam
    */
    public function setStraatnaam($straatnaam): void
    {
        $this->straatnaam = $straatnaam;
    }

    /**
     * Returns the huisnummer
     *
     * @return string $huisnummer
     */
    public function getHuisnummer(): string
    {
        return $this->huisnummer;
    }

    /**
     * Sets the huisnummer
     *
     * @param string $huisnummer
     */
    public function setHuisnummer($huisnummer): void
    {
        $this->huisnummer = $huisnummer;
    }

    /**
     * Returns the postcode
     *
     * @return string $postcode
     */
    public function getpostcode(): string
    {
        return $this->postcode;
    }

    /**
     * Sets the postcode
     *
     * @param string $postcode
     */
    public function setPostcode($postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * Returns the woonplaats
     *
     * @return string $woonplaats
     */
    public function getWoonplaats(): string
    {
        return $this->woonplaats;
    }

    /**
     * Sets the woonplaats
     *
     * @param string $woonplaats
     */
    public function setWoonplaats($woonplaats): void
    {
        $this->woonplaats = $woonplaats;
    }

    /**
     * Returns the land
     *
     * @return string $land
     */
    public function getLand(): string
    {
        return $this->land;
    }

    /**
     * Sets the land
     *
     * @param string $land
     */
    public function setLand($land): void
    {
        $this->land = $land;
    }

    /**
     * Returns the telefoonnr_vast
     *
     * @return string $telefoonnr_vast
     */
    public function getTelefoonnrVast(): string
    {
        return $this->telefoonnr_vast;
    }

    /**
     * Sets the telefoonnr_vast
     *
     * @param string $telefoonnr_vast
     */
    public function setTelefoonnrVast($telefoonnr_vast): void
    {
        $this->telefoonnr_vast = $telefoonnr_vast;
    }

    /**
     * Returns the tel_op_website
     *
     * @return string $tel_op_website
     */
    public function getTelOpWebsite(): string
    {
        return $this->tel_op_website;
    }

    /**
     * Sets the tel_op_website
     *
     * @param string $tel_op_website
     */
    public function setTelOpWebsite($tel_op_website): void
    {
        $this->tel_op_website = $tel_op_website;
    }

    /**
     * Returns the adres_op_website
     *
     * @return string $adres_op_website
     */
    public function getAdresOpWebsite(): string
    {
        return $this->adres_op_website;
    }

    /**
     * Sets the adres_op_website
     *
     * @param string $adres_op_website
     */
    public function setAdresOpWebsite($adres_op_website): void
    {
        $this->adres_op_website = $adres_op_website;
    }

    /**
     * Returns the deleted
     *
     * @return int $deleted
     */
    public function getDeleted(): int
    {
        return $this->deleted;
    }

    /**
     * Sets the deleted
     *
     * @param int $deleted
     */
    public function setDeleted($deleted): void
    {
        $this->deleted = $deleted;
    }
   
    /**
     * Returns the partnerdeleted
     *
     * @return int|null $partnerdeleted
     */
    public function getPartnerdeleted(): ?int
    {
        return $this->partnerdeleted;
    }

    /**
     * Sets the partnerdeleted
     *
     * @param int $partnerdeleted
     */
    public function setPartnerdeleted($partnerdeleted): void
    {
        $this->partnerdeleted = $partnerdeleted;
    }
   
    /**
     * Returns the vaderdeleted
     *
     * @return int|null $vaderdeleted
     */
    public function getVaderdeleted(): ?int
    {
        return $this->vaderdeleted;
    }

    /**
     * Sets the vaderdeleted
     *
     * @param int $vaderdeleted
     */
    public function setVaderdeleted($vaderdeleted): void
    {
        $this->vaderdeleted = $vaderdeleted;
    }
   
    /**
     * Returns the moederdeleted
     *
     * @return int|null $moederdeleted
     */
    public function getMoederdeleted(): ?int
    {
        return $this->moederdeleted;
    }

    /**
     * Sets the moederdeleted
     *
     * @param int $moederdeleted
     */
    public function setMoederdeleted($moederdeleted): void
    {
        $this->moederdeleted = $moederdeleted;
    }

    /**
     * Returns the boolean state of deleted
     *
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Returns the postcodeorg
     *
     * @return string $postcodeorg
     */
    public function getPostcodeorg(): string
    {
        return $this->postcodeorg;
    }

    /**
     * Sets the postcodeorg
     *
     * @param string $postcodeorg
     */
    public function setPostcodeorg($postcodeorg): void
    {
        $this->postcodeorg = $postcodeorg;
    }

    /**
     * Returns the huisnummerorg
     *
     * @return string $huisnummerorg
     */
    public function getHuisnummerorg(): string
    {
        return $this->huisnummerorg;
    }

    /**
     * Sets the huisnummerorg
     *
     * @param string $huisnummerorg
     */
    public function setHuisnummerorg($huisnummerorg): void
    {
        $this->huisnummerorg = $huisnummerorg;
    }

    /**
     * Returns the woonplaatsorg
     *
     * @return string woonplaatsorg
     */
    public function getWoonplaatsorg(): string
    {
        return $this->woonplaatsorg;
    }

    /**
     * Sets the woonplaatsorg
     *
     * @param string $woonplaatsorg
     */
    public function setWoonplaatsorg($woonplaatsorg): void
    {
        $this->woonplaatsorg = $woonplaatsorg;
    }

    /**
     * Returns the straatnaamorg
     *
     * @return string straatnaamorg
     */
    public function getStraatnaamorg(): string
    {
        return $this->straatnaamorg;
    }

    /**
     * Sets the straatnaamorg
     *
     * @param string $straatnaamorg
     */
    public function setStraatnaamorg($straatnaamorg): void
    {
        $this->straatnaamorg = $straatnaamorg;
    }

    /**
     * Returns the telefoonorg
     *
     * @return string telefoonorg
     */
    public function getTelefoonorg(): string
    {
        return $this->telefoonorg;
    }

    /**
     * Sets the telefoonorg
     *
     * @param string $telefoonorg
     */
    public function setTelefoonorg($telefoonorg): void
    {
        $this->telefoonorg = $telefoonorg;
    }

    /**
     * Returns the partnerorg
     *
     * @return string partnerorg
     */
    public function getPartnerorg(): string
    {
        return $this->partnerorg;
    }

    /**
     * Sets the partnerorg
     *
     * @param string $partnerorg
     */
    public function setPartnerorg($partnerorg): void
    {
        $this->partnerorg = $partnerorg;
    }

    /**
     * Returns the burgerlijke_staatorg
     *
     * @return string burgerlijke_staatorg
     */
    public function getBurgerlijkeStaatorg(): string
    {
        return $this->burgerlijke_staatorg;
    }

    /**
     * Sets the burgerlijke_staatorg
     *
     * @param string $burgerlijke_staatorg
     */
    public function setBurgerlijkeStaatorg($burgerlijke_staatorg): void
    {
        $this->burgerlijke_staatorg = $burgerlijke_staatorg;
    }


}