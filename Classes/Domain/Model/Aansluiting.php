<?php
namespace Parousia\Churchpersreg\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***
 *
 * This file is part of the "Churchperesreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * Aansluiting is a request to become part of the church
 */
class Aansluiting extends AbstractEntity
{
    /**
     * datumaanvraag
     *
     * @var datetime
     */
    protected $datumaanvraag = '';

     /**
     * voornaam
     *
     * @var string
     */
    protected $voornaam = '';

  /**
     * achternaam
     *
     * @var string
     */
    protected $achternaam = '';

    /**
     * geboortedatum
     *
     * @var string
     */
    protected $geboortedatum = NULL;

    /**
     * straatnaam
     *
     * @var string
     */

    protected $straatnaam = '';

    /**
     * huisnummer
     *
     * @var string
     */

    protected $huisnummer = '';

    /**
     * postcode
     *
     * @var string
     */

    protected $postcode = '';

    /**
     * woonplaats
     *
     * @var string
     */
    protected $woonplaats = '';

    /**
     * telefoonnummer
     *
     * @var string
     */
    protected $telefoonnummer = NULL;

    /**
     * mobieltelnummer
     *
     * @var string
     */
    protected $mobieltelnummer = '';

    /**
     * emailadres
     *
     * @var string
     */
    protected $emailadres = '';

    /**
     * partnernaam
     *
     * @var string
     */
    protected $partnernaam = '';

    /**
     * namenkinderen
     *
     * @var string
     */
    protected $namenkinderen = '';

    /**
     * id_bezoeker
     *
     * @var string
     */
    protected $id_bezoeker = '';

/**
* Getters and Setters
*/

    /**
     * Returns the voornaam
     *
     * @return string $voornaam
     */
    public function getVoornaam(): string
    {
        return $this->voornaam;
    }
    /**
     * Sets the voornaam
     *
     * @param string $voornaam
     */
    public function setVoornaam($voornaam): void
    {
        $this->voornaam = $voornaam;
    }

   /**
     * Returns the achternaam
     *
     * @return string $achternaam
     */
    public function getAchternaam(): string
    {
        return $this->achternaam;
    }

    /**
     * Sets the achternaam
     *
     * @param string $achternaam
     */
    public function setAchternaam($achternaam): void
    {
        $this->achternaam = $achternaam;
    }

    /**
     * Returns the emailadres
     *
     * @return string $emailadres
     */
    public function getEmailadres(): string
    {
        return $this->emailadres;
    }

    /**
     * Sets the emailadres
     *
     * @param string $emailadres
     */
    public function setEmailadres($emailadres): void
    {
        $this->emailadres = $emailadres;
    }

    /**
     * Returns the mobieltelnummer
     *
     * @return string $mobieltelnummer
     */
    public function getMobieltelnummer(): string
    {
        return $this->mobieltelnummer;
    }

    /**
     * Sets the mobieltelnummer
     *
     * @param string $mobieltelnummer
     */
    public function setMobieltelnummer($mobieltelnummer): void
    {
        $this->mobieltelnummer = $mobieltelnummer;
    }

   /**
     * Returns the 
     *geboortedatum
     * @return string|null $geboortedatum
    */
    public function getGeboortedatum(): ?string
    {
        return $this->geboortedatum;
    }
	/**
     * Sets the geboortedatum
     *
     * @param \DateTime $geboortedatum
     * @return void
    */
    public function setGeboortedatum($geboortedatum): void
    {
        $this->geboortedatum = $geboortedatum;
    }

   /**
     * Returns the datumaanvraag
     *
     * @return string|NULL $datumaanvraag
    */
    public function getDatumaanvraag(): ?string
    {
        return $this->datumaanvraag;
    }
	/**
     * Sets the datumaanvraag
     *
     * @param \DateTime $datumaanvraag
     * @return void
    */
    public function setDatumaanvraag($datumaanvraag): void
    {
        $this->datumaanvraag = $datumaanvraag;
    }

    /**
     * Returns the bezoeker
     *
     * @return string $bezoeker
     */
    public function getBezoeker(): string
    {
        return $this->bezoeker;
    }

    /**
     * Sets the bezoeker
     *
     * @param string $bezoeker
     */
    public function setBezoeker($bezoeker): void
    {
        $this->bezoeker = $bezoeker;
    }

   /**
     * Sets the namenkinderen
     *
     * @param string $namenkinderen
     */
    public function setNamenkinderen($namenkinderen): void
    {
        $this->namenkinderen = $namenkinderen;
    }

    /**
     * Returns the namenkinderen
     *
     * @return string $namenkinderen
     */
    public function getNamenkinderen(): string
    {
        return $this->namenkinderen;
    }

    /**
     * Returns the partnernaam
     *
     * @return string|null $partnernaam
     */
    public function getPartnernaam(): ?string
    {
        return $this->partnernaam;
    }

    /**
     * Sets the partnernaam
     *
     * @param string $partnernaam
    */
    public function setPartnernaam($partnernaam): void
    {
        $this->partnernaam = $partnernaam;
	}

    /**
     * Returns the straatnaam
     *
     * @return string $straatnaam
     */
    public function getStraatnaam(): string
    {
        return $this->straatnaam;
    }

    /**
     * Sets the straatnaam
     *
     * @param string $straatnaam
    */
    public function setStraatnaam($straatnaam): void
    {
        $this->straatnaam = $straatnaam;
    }

    /**
     * Returns the huisnummer
     *
     * @return string $huisnummer
     */
    public function getHuisnummer(): string
    {
        return $this->huisnummer;
    }

    /**
     * Sets the huisnummer
     *
     * @param string $huisnummer
     */
    public function setHuisnummer($huisnummer): void
    {
        $this->huisnummer = $huisnummer;
    }

    /**
     * Returns the postcode
     *
     * @return string $postcode
     */
    public function getpostcode(): string
    {
        return $this->postcode;
    }

    /**
     * Sets the postcode
     *
     * @param string $postcode
     */
    public function setPostcode($postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * Returns the woonplaats
     *
     * @return string $woonplaats
     */
    public function getWoonplaats(): string
    {
        return $this->woonplaats;
    }

    /**
     * Sets the woonplaats
     *
     * @param string $woonplaats
     */
    public function setWoonplaats($woonplaats): void
    {
        $this->woonplaats = $woonplaats;
    }

    /**
     * Returns the telefoonnummer
     *
     * @return string|null $telefoonnummer
     */
    public function getTelefoonnummer(): ?string
    {
        return $this->telefoonnummer;
    }

    /**
     * Sets the telefoonnummer
     *
     * @param string $telefoonnummer
     */
    public function setTelefoonnummer($telefoonnummer): void
    {
        $this->telefoonnummer = $telefoonnummer;
    }

    /**
     * Returns the deleted
     *
     * @return bool $deleted
     */
    public function getDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Sets the deleted
     *
     * @param bool $deleted
     */
    public function setDeleted($deleted): void
    {
        $this->deleted = $deleted;
    }
}