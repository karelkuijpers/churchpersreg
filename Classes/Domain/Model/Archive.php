<?php
namespace Parousia\Churchpersreg\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***
 *
 * This file is part of the "Churchperesreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * Account is a request to become part of the church
 */
class Archive extends AbstractEntity
{
     /**
     * idparentarchive
     *
     * @var int
     */
    protected $idparentarchive = 0;

  /**
     * idperson
     *
     * @var int
     */
    protected $idperson = 0;

  /**
     * idpersonowner
     *
     * @var int
     */
    protected $idpersonowner = 0;

    /**
     * subject
     *
     * @var string
     */
    protected $subject = NULL;

    /**
     * typecontent
     *
     * @var string
     */
    protected $typecontent = NULL;

    /**
     * username
     *
     * @var string
     */
    protected $username = '';

    /**
     * author
     *
     * @var string
     */
    protected $author = '';

    /**
     * date
     *
     * @var string|NULL
     */
    protected $date = NULL;

    /**
     * documentname
     *
     * @var string|NULL
     */
    protected $documentname = '';

    /**
     * content
     *
     * @var string
     */

    protected $content = '';

    /**
     * attachements
     *
     * @var string
     */

    protected $attachements = '';

    /**
     * confidential
     *
     * @var int
     */
    protected $confidential=0;

/**
* Getters and Setters
*/

    /**
     * Returns the idparentarchive
     *
     * @return int|NULL $idparentarchive
     */
    public function getIdparentarchive(): ?int
    {
        return $this->idparentarchive;
    }
    /**
     * Sets the idparentarchive
     *
     * @param int $idparentarchive
     */
    public function setIdparentarchive($idparentarchive): void
    {
        $this->idparentarchive = $idparentarchive;
    }

   /**
     * Returns the idperson
     *
     * @return int|NULL $idperson
     */
    public function getIdperson(): ?int
    {
        return $this->idperson;
    }

    /**
     * Sets the idperson
     *
     * @param int $idperson
     */
    public function setIdperson($idperson): void
    {
        $this->idperson = $idperson;
    }

   /**
     * Returns the idpersonowner
     *
     * @return int $idpersonowner
     */
    public function getIdpersonowner(): int
    {
        return $this->idpersonowner;
    }

    /**
     * Sets the idpersonowner
     *
     * @param int $idpersonowner
     */
    public function setIdpersonowner($idpersonowner): void
    {
        $this->idpersonowner = $idpersonowner;
    }

   /**
     * Returns the subject
     *
     * @return string|null $subject
    */
    public function getSubject(): ?string
    {
        return $this->subject;
    }
	/**
     * Sets the subject
     *
     * @param string $subject
     * @return void
    */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

   /**
     * Returns the typecontent
     *
     * @return string $typecontent
    */
    public function getTypecontent(): string
    {
        return $this->typecontent;
    }
	/**
     * Sets the typecontent
     *
     * @param string $typecontent
     * @return void
    */
    public function setTypecontent($typecontent): void
    {
        $this->typecontent = $typecontent;
    }

    /**
     * Returns the username
     *
     * @return string $username
     */
    public function getusername(): string
    {
        return $this->username;
    }

    /**
     * Sets the username
     *
     * @param string $username
    */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * Returns the author
     *
     * @return string $author
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * Sets the author
     *
     * @param string $author
    */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

    /**
     * Returns the date
     *
     * @return string|NULL $date
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * Sets the date
     *
     * @param string $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * Returns the documentname
     *
     * @return string|NULL $documentname
     */
    public function getDocumentname(): ?string
    {
        return $this->documentname;
    }

    /**
     * Sets the documentname
     *
     * @param string $documentname
     */
    public function setDocumentname($documentname): void
    {
        $this->documentname = $documentname;
    }

    /**
     * Returns the content
     *
     * @return string $content
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Sets the content
     *
     * @param string $content
    */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * Returns the attachements
     *
     * @return string $attachements
     */
    public function getAttachements(): string
    {
        return $this->attachements;
    }

    /**
     * Sets the attachements
     *
     * @param string $attachements
    */
    public function setAttachements($attachements): void
    {
        $this->attachements = $attachements;
    }

    /**
     * Get confidential
     *
     * @return int
     */
    public function getConfidential(): int
    {
        return $this->confidential;
    }

    /**
     * Set confidential
     *
     * @param int $confidential
     */
    public function setConfidential($confidential): void
    {
        $this->confidential = $confidential;
    }

    /**
     * Set uid
     *
     * @param int $uid
     */
    public function setUid($uid): void
    {
        $this->uid = $uid;
    }

    /**
     * Returns the deleted
     *
     * @return bool $deleted
     */
    public function getDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Sets the deleted
     *
     * @param bool $deleted
     */
    public function setDeleted($deleted): void
    {
        $this->deleted = $deleted;
    }
}