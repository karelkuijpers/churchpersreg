<?php
namespace Parousia\Churchpersreg\Domain\Model;

/**
 * This file is part of the "churchpersreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Persoon search object which holds all information to get the correct
 * persoon records.
 *
 */
class Search extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * Basic search word
     *
     * @var string
     */
    protected $general='';

    /**
     * Search peradres
     *
     * @var bool
     */
    protected $peradres=false;

    /**
     * voornaam
     *
     * @var string
     */
    protected $voornaam='';

    /**
     * achternaam
     *
     * @var string
     */
    protected $achternaam='';

    /**
     * postocde
     *
     * @var string
     */
    protected $postcode='';

    /**
     * woonplaats
     *
     * @var string
     */
    protected $woonplaats='';

    /**
     * bezoeker
     *
    /** @var string */
    protected $bezoeker="is bezoeker";

    /**
     * lid
     *
    /** @var string */
    protected $lid="is geen lid";

    /**
     * inclverwijderd
     *
    /** @var bool */
    protected $inclverwijderd=false;

    /**
     * leeftijfmin
     *
    /** @var string */
    protected $leeftijdmin='';

    /**
     * leeftijfmax
     *
    /** @var string */
    protected $leeftijdmax='';

    /**
     * inputdatmin
     *
    /** @var string */
    protected $inputdatmin=NULL;

    /**
     * inputdatmax
     *
    /** @var string */
    protected $inputdatmax=NULL;

    /**
     * wijzdatmin
     *
    /** @var string */
    protected $wijzdatmin=NULL;

    /**
     * wijzdatmax
     *
    /** @var string */
    protected $wijzdatmax=NULL;

    /**
     * liddatmin
     *
    /** @var string */
    protected $liddatmin=NULL;

    /**
     * liddatmax
     *
    /** @var string */
    protected $liddatmax=NULL;


    /**
     * Get the general
     *
     * @return string
     */
    public function getGeneral(): string
    {
        return $this->general;
    }

    /**
     * Set general
     *
     * @param string $general
     */
    public function setGeneral($general): void
    {
        $this->general = $general;
    }

    /**
     * Get peradres
     *
     * @return bool
     */
    public function getPeradres(): bool
    {
        return $this->peradres;
    }

    /**
     * Set peradres
     *
     * @param bool $peradres
     */
    public function setPeradres($peradres): void
    {
        $this->peradres = $peradres;
    }

    /**
     * @param string $achternaam
     */
    public function setAchternaam($achternaam): void
    {
        $this->achternaam = $achternaam;
    }

    /**
     * @return string
     */
    public function getAchternaam(): string
    {
        return $this->achternaam;
    }

    /**
     * @param string $voornaam
     */
    public function setVoornaam($voornaam): void
    {
        $this->voornaam = $voornaam;
    }

    /**
     * @return string
     */
    public function getvoornaam(): string
    {
        return $this->voornaam;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * @param string $woonplaats
     */
    public function setWoonplaats($woonplaats): void
    {
        $this->woonplaats = $woonplaats;
    }

    /**
     * @return string
     */
    public function getWoonplaats(): string
    {
        return $this->woonplaats;
    }

    /**
     * @param string $leeftijdmin
     */
	public function setLeeftijdmin($leeftijdmin): void
    {
        $this->leeftijdmin = $leeftijdmin;
    }

    /**
     * @return string
     */
    public function getLeeftijdmin(): string
    {
        return $this->leeftijdmin;
    }

    /**
     * @param string $leeftijdmax
     */
	public function setLeeftijdmax($leeftijdmax): void
    {
        $this->leeftijdmax = $leeftijdmax;
    }

    /**
     * @return string
     */
    public function getLeeftijdmax(): string
    {
        return $this->leeftijdmax;
    }

    /**
     * @param string $inputdatmin
     */
    public function setInputdatmin($inputdatmin): void
    {
        $this->inputdatmin = $inputdatmin;
    }

    /**
     * @return string|NULL
     */
    public function getInputdatmin(): ?string
    {
        return $this->inputdatmin;
    }

    /**
     * @param string $inputdatmax
     */
    public function setInputdatmax($inputdatmax): void
    {
        $this->inputdatmax = $inputdatmax;
    }

    /**
     * @return string|null
     */
    public function getInputdatmax(): ?string
    {
        return $this->inputdatmax;
    }

    /**
     * @param string $liddatmin
     */
    public function setLiddatmin($liddatmin): void
    {
        $this->liddatmin = $liddatmin;
    }

    /**
     * @return string|null
     */
    public function getLiddatmin(): ?string
    {
        return $this->liddatmin;
    }

    /**
     * @param string $liddatmax
     */
    public function setLiddatmax($liddatmax): void
    {
        $this->liddatmax = $liddatmax;
    }

    /**
     * @return string|null
     */
    public function getLiddatmax(): ?string
    {
        return $this->liddatmax;
    }

    /**
     * @param string $wijzdatmin
     */
    public function setWijzdatmin($wijzdatmin): void
    {
        $this->wijzdatmin = $wijzdatmin;
    }

    /**
     * @return string|null
     */
    public function getWijzdatmin(): ?string
    {
        return $this->wijzdatmin;
    }

    /**
     * @param string $wijzdatmax
     */
    public function setWijzdatmax($wijzdatmax): void
    {
        $this->wijzdatmax = $wijzdatmax;
    }

    /**
     * @return string|null
     */
    public function getWijzdatmax(): ?string
    {
        return $this->wijzdatmax;
    }

    /**
     * @return string
     */
    public function getBezoeker(): string
    {
        return $this->bezoeker;
    }
	
    /**
     * @param string $bezoeker
     */
    public function setBezoeker($bezoeker): void
    {
        $this->bezoeker = $bezoeker;
    }

    /**
     * @return string
     */
    public function getLid(): string
    {
        return $this->lid;
    }
	
    /**
     * @param string $lid
     */
    public function setLid($lid): void
    {
        $this->lid = $lid;
    }

    /**
     * @return bool
     */
    public function getInclverwijderd(): bool
    {
        return $this->inclverwijderd;
    }

    /**
     * @param bool $inclverwijderd
     */
    public function setInclverwijderd($inclverwijderd)
    {
        $this->inclverwijderd = $inclverwijderd;
    }
}
