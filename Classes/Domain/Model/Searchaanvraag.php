<?php
namespace Parousia\Churchpersreg\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***
 *
 * This file is part of the "Churchperesreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * Searchaanvraag is a request to become part of the church
 */
class Searchaanvraag extends AbstractEntity
{
     /**
     * general
     *
     * @var string
     */
    protected $general = '';

  /**
     * typerequest
     *
     * @var string
     */
    protected $typerequest = '';

    /**
     * openonly	
     *
     * @var int
     */

    protected $openonly = 1;

    /**
     * huisnummer
     *
     * @var string
     */
/**
* Getters and Setters
*/

    /**
     * Returns the general
     *
     * @return string $general
     */
    public function getGeneral(): string
    {
        return $this->general;
    }
    /**
     * Sets the general
     *
     * @param string $general
     */
    public function setGeneral($general): void
    {
        $this->general = $general;
    }

   /**
     * Returns the typerequest
     *
     * @return string $typerequest
     */
    public function getTyperequest(): string
    {
        return $this->typerequest;
    }

    /**
     * Sets the typerequest
     *
     * @param string $typerequest
     */
    public function setTyperequest($typerequest): void
    {
        $this->typerequest = $typerequest;
    }

    /**
     * Returns the openonly
     *
     * @return int|null $openonly
     */
    public function getOpenonly(): ?int
    {
        return $this->openonly;
    }

    /**
     * Sets the openonly
     *
     * @param int $openonly
    */
    public function setOpenonly($openonly): void
    {
        $this->openonly = $openonly;
    }
}