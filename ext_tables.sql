
#
# Table structure for table 'adres'
#
CREATE TABLE `adres` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `straatnaam` varchar(80) NOT NULL DEFAULT '',
  `huisnummer` varchar(20) NOT NULL DEFAULT '',
  `postcode` varchar(10) NOT NULL DEFAULT '',
  `woonplaats` varchar(40) NOT NULL DEFAULT '',
  `land` varchar(32) NOT NULL,
  `telefoonnr_vast` tinyblob NOT NULL,
  `tel_op_website` enum('telefoon tonen','telefoon niet tonen') DEFAULT 'telefoon tonen',
  `adres_op_website` enum('adres tonen','adres niet tonen') DEFAULT 'adres tonen',
  `steller` int(11) NOT NULL DEFAULT '0',
  `datum_wijziging` datetime NULL DEFAULT NULL,
  # ON UPDATE CURRENT_TIMESTAMP,
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) DEFAULT CHARACTER SET utf8mb4;

#
# Table structure for table 'bediening'
#

CREATE TABLE `bediening` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `omschrijving` varchar(255) NOT NULL DEFAULT '',
  `id_parent` int(11) NULL DEFAULT NULL,
  `id_bedieningsleider` varchar(255) NOT NULL DEFAULT '0',
  `soort` enum('afdeling','bediening') NOT NULL DEFAULT 'afdeling',
  `secretariaat` varchar(255) NOT NULL DEFAULT '',
  `taakvisie` mediumtext,
  `toekomstverwachting` mediumtext,
  `PlanActiviteiten` mediumtext,
  `VolgendeJaren` mediumtext,
  PRIMARY KEY (`uid`)
) DEFAULT CHARACTER SET utf8mb4 ;

#
# Table structure for table 'contact_archive'
#
CREATE TABLE `contact_archive` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_parentarchive` int(11) NOT NULL DEFAULT '0',
  `id_person` int(11) NOT NULL,
  `subject` tinyblob NOT NULL,
  `author` varchar(64) NOT NULL,
  `id_person_owner` int(11) NOT NULL,
  `date` date NOT NULL,
  `document_name` tinyblob DEFAULT NULL,
  `content` mediumblob NOT NULL,
  `type_content` enum('email','notitie','bestand') NOT NULL DEFAULT 'email',
  `confidential` tinyint(1) NOT NULL,
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
) DEFAULT CHARACTER SET utf8mb4;

#
# Table structure for table 'persoon'
#
CREATE TABLE `persoon` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `achternaam` varchar(50) NOT NULL DEFAULT '',
  `tussenvoegsel` varchar(10) NOT NULL DEFAULT '',
  `voornamen` varchar(32) NOT NULL DEFAULT '',
  `roepnaam` varchar(20) NOT NULL DEFAULT '',
  `titel` varchar(20) NOT NULL DEFAULT '',
  `mobieltelnr` tinyblob NOT NULL,
  `emailadres` tinyblob NOT NULL,
  `beroep` varchar(100) NOT NULL DEFAULT '',
  `geslacht` enum('man','vrouw') DEFAULT NULL,
  `burgerlijke_staat` enum('ongehuwd','gehuwd','samenwonend','gescheiden','weduwnaar','weduwe','overleden','verbroken partnerschap') DEFAULT NULL,
  `geboortedatum` date NULL DEFAULT NULL,
  `geboorteplaats` varchar(40) NOT NULL DEFAULT '',
  `trouwdatum` date NULL DEFAULT NULL,
  `id_partner` int(11) NOT NULL DEFAULT '0',
  `id_vader` int(11) NOT NULL DEFAULT '0',
  `email_op_website` enum('email tonen','email niet tonen') DEFAULT 'email tonen',
  `bezoeker` enum('is bezoeker','is geen bezoeker') DEFAULT 'is bezoeker',
  `mobiel_op_website` enum('mobiel tonen','mobiel niet tonen') DEFAULT 'mobiel tonen',
  `gedoopt` enum('is gedoopt','niet gedoopt') DEFAULT NULL,
  `doopdatum` date NULL DEFAULT NULL,
  `doopplaats` varchar(40) NOT NULL DEFAULT '',
  `doopgemeente` varchar(50) NOT NULL DEFAULT '',
  `lid` enum('is lid','is geen lid') DEFAULT NULL,
  `datum_lidmaatschap` date NULL DEFAULT NULL,
  `id_adres` tinyblob,
  `id_moeder` int(11) NOT NULL DEFAULT '0',
  `steller` int(11) NOT NULL DEFAULT '0',
  `datum_wijziging` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datumoverlijden` date NULL DEFAULT NULL,
  `foto` mediumblob NULL DEFAULT NULL,
  `mimetypefoto` varchar(30) NULL DEFAULT NULL,
  `photoWidth` int(6) NULL DEFAULT NULL,
  `photoHeight` int(6) NULL DEFAULT NULL,
  `thumbnail` mediumblob NULL DEFAULT NULL,
  `geboortedatum_op_intranet` tinyint(1) NOT NULL DEFAULT '1',
  `gebruik_naam_partner` enum('ja','nee','beide') NOT NULL DEFAULT 'nee',
  `invoerdatum` date NULL DEFAULT NULL,
  `cleanteam` enum('kan meedoen','kan niet meedoen') NOT NULL DEFAULT 'kan meedoen',
  `verwijderreden` smallint(2) NOT NULL DEFAULT '0', 
  #COMMENT "0:onbekend,1:Verhuisd,2:Overleden,3:Onvrede,5:Overgestapt naar andere gemeente,6:Geloof  kwijtgeraakt,7:Afgehaakt",
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',  
  PRIMARY KEY (`uid`),
  KEY `id_partner` (`id_partner`),
  KEY `id_vader` (`id_vader`)
)  DEFAULT CHARACTER SET utf8mb4;

#
# Table structure for table 'taak'
#
CREATE TABLE `taak` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `omschrijving` varchar(255) NOT NULL DEFAULT '',
  `verantwoordelijkheden` blob NOT NULL,
  `verlangen` varchar(255) NOT NULL DEFAULT '',
  `gaven` mediumtext NOT NULL,
  `organisatiestijl` enum('Gestructureerd','Ongestructureerd') NULL DEFAULT NULL,
  `energiestijl` enum('Taakgericht','Mensgericht') NULL DEFAULT NULL,
  `vaardigheden` varchar(255) NOT NULL DEFAULT '',
  `geestelijke_rijpheid` enum('zoekend','groeiend','stabiel','leider') NOT NULL DEFAULT 'zoekend',
  `lidmaatschap` enum('is lid','geen lid') NOT NULL DEFAULT 'geen lid',
  `duur` smallint(1) NOT NULL DEFAULT '0',
  `tijdsbeslag` varchar(128) NOT NULL DEFAULT '0',
  `verplichtingen` varchar(255) NOT NULL DEFAULT '',
  `aantal` int(4) NOT NULL DEFAULT '0',
  `opmerkingen` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`)
)  DEFAULT CHARACTER SET utf8mb4;

#
# Table structure for table 'taakbekleding'
#
CREATE TABLE `taakbekleding` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `id_persoon` int(11) NOT NULL DEFAULT '0',
  `datum_start` date NULL DEFAULT NULL,
  `datum_einde` date NULL DEFAULT NULL,
  `opmerkingen` blob NOT NULL,
  PRIMARY KEY (`uid`)
)  DEFAULT CHARACTER SET utf8mb4;

#
# Table structure for table 'transportusername'
#
CREATE TABLE `transportusername` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL UNIQUE,
  `user_password` varchar(255) NULL DEFAULT NULL,
  `date_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `totalsent` int DEFAULT 0,
  PRIMARY KEY (`uid`)
)  DEFAULT CHARACTER SET utf8mb4;

#
# Table structure for table 'aanvraag'
#
CREATE TABLE `aanvraag` (
  `uidpersoon` int(11) NOT NULL,
  `typerequest` enum('aansluiting','lidmaatschap','doop','kring') NOT NULL DEFAULT 'aansluiting',
  `datumaanvraag` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  'uidpartner' int(11) NULL,
  `id_bezoeker` varchar(255) DEFAULT 0,
  `deleted` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`uidpersoon`,`typerequest`)
)  DEFAULT CHARACTER SET utf8mb4;

#
# Table structure for table 'tx_formtodatabase_domain_model_formresult'
#
CREATE TABLE tx_formtodatabase_domain_model_formresult (
	`id_bezoeker` varchar(255) DEFAULT 0,
);

#
# Table structure for table 'aansluiting'
#
CREATE TABLE `aansluiting` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `datumaanvraag` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `voornaam` varchar(32) NOT NULL DEFAULT '',
  `achternaam` varchar(60) NOT NULL DEFAULT '',
  `geboortedatum` date NULL DEFAULT NULL,
  `straatnaam` varchar(80) NOT NULL DEFAULT '',
  `huisnummer` varchar(20) NOT NULL DEFAULT '',
  `postcode` varchar(10) NOT NULL DEFAULT '',
  `woonplaats` varchar(40) NOT NULL DEFAULT '',
  `telefoonnummer` tinyblob NOT NULL,
  `mobieltelnummer` tinyblob NOT NULL,
  `emailadres` tinyblob NOT NULL,
  `partnernaam` varchar(72) NULL,
  `namenkinderen` varchar(256) NULL,
  `id_bezoeker` varchar(255) DEFAULT 0,
  `deleted` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`uid`)
)  DEFAULT CHARACTER SET utf8mb4;


#
# Table structure for table 'updatelog'
#
CREATE TABLE `updatelog` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `date_change` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `id_author` int(11) NOT NULL DEFAULT '0',
  `tablename` varchar(32) NOT NULL DEFAULT '',
  `uid_table`int(11) NULL DEFAULT NULL,
  `changedvalue`  json NULL DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `date_change` (`date_change`),

)  DEFAULT CHARACTER SET utf8mb4;
