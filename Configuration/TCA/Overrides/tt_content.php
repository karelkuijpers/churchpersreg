<?php
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
defined('TYPO3') or die();

/***************
 * Plugin
 */
ExtensionManagementUtility::addTcaSelectItemGroup(
    'tt_content',
    'list_type',
    'churchpersreg',
    'LLL:EXT:churchpersreg/Resources/Private/Language/locallang_db.xlf:tt_content.list_type_group'
);
$pluginConfig = ['PersoonSearch','PersoonDetail','PersoonEmail','PersoonLedenlijst','PersoonAdressenlijst','AanvraagAansluiting','AanvraagSend','AanvraagSearch','UpdatelogSearch'];
foreach ($pluginConfig as $pluginName) {
//    $pluginNameForLabel = $pluginName === 'pi1' ? 'news_list' : $pluginName;
    $pluginSignature = ExtensionUtility::registerPlugin(
        'Churchpersreg',
        $pluginName,
        'LLL:EXT:churchpersreg/Resources/Private/Language/locallang_be.xlf:plugin.' . $pluginName . '.title',
        'content-plugin-churchpersreg-'.strtolower($pluginName),
        'churchpersreg',
    );
	if ($pluginName=='AanvraagSend' or $pluginName=='AanvraagAansluiting')
	{
	    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
		$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key';
		if ($pluginName=='AanvraagSend')
			ExtensionManagementUtility::addPiFlexFormValue(
	        $pluginSignature,
        	'FILE:EXT:churchpersreg/Configuration/FlexForms/flexform_Aanvraag.xml'
	    	);
		else
			ExtensionManagementUtility::addPiFlexFormValue(
	        $pluginSignature,
        	'FILE:EXT:churchpersreg/Configuration/FlexForms/flexform_Aansluiting.xml'
	    	);				
	}
}

//$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'recursive';
//$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
//ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
//    'FILE:EXT:churchpersreg/Configuration/FlexForms/flexform_persoon.xml'); 

//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToInsertRecords('persoon');
