<?php
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;
use Parousia\Churchpersreg\Controller\PersoonController;
use Parousia\Churchpersreg\Controller\AansluitingController;

defined('TYPO3') || die('Access denied.');

$boot = static function (): void {
	ExtensionUtility::configurePlugin(
        'Churchpersreg',
        'PersoonSearch',
		[PersoonController::Class => 'searchForm,selectedList,detail,edit,delete,email,sendEmail,contactInfoReg,export,contactInfoReg,contactInfoSave,deleteArchive'],
		[PersoonController::Class => 'searchForm,selectedList,detail,edit,delete,email,sendEmail,contactInfoReg,export,contactInfoReg,contactInfoSave,deleteArchive']
    );
	ExtensionUtility::configurePlugin(
        'Churchpersreg',
        'PersoonDetail',
		[PersoonController::Class => 'detail'],
		[PersoonController::Class => 'detail']
    );
		ExtensionUtility::configurePlugin(
        'Churchpersreg',
        'PersoonEmail',
		[PersoonController::Class => 'email,sendEmail'],
		[PersoonController::Class => 'email,sendEmail']
    );
	ExtensionUtility::configurePlugin(
        'Churchpersreg',
        'PersoonLedenlijst',
		[PersoonController::Class => 'ledenlijst'],
		[PersoonController::Class => 'ledenlijst']
    );
	ExtensionUtility::configurePlugin(
        'Churchpersreg',
        'PersoonAdressenlijst',
		[PersoonController::Class => 'adressenlijst'],
		[PersoonController::Class => 'adressenlijst']
    );
	ExtensionUtility::configurePlugin(
        'Churchpersreg',
        'AanvraagAansluiting',
		[AansluitingController::Class => 'aansluitingSend,saveAansluiting,aansluitingConfirmation'],
		[AansluitingController::Class => 'aansluitingSend,saveAansluiting,aansluitingConfirmation']
    );
	ExtensionUtility::configurePlugin(
        'Churchpersreg',
        'AanvraagSend',
		[PersoonController::Class => 'requestSend,saveRequest'],
		[PersoonController::Class => 'requestSend,saveRequest']
    );
	ExtensionUtility::configurePlugin(
        'Churchpersreg',
        'AanvraagSearch',
		[PersoonController::Class => 'searchAanvragen,selectedListAanvragen,detailaanvraag,saveAanvraag,deleteAanvraag,detail,edit,delete,email,sendEmail,export,contactInfoReg,contactInfoSave,deleteArchive'],
		[PersoonController::Class => 'searchAanvragen,selectedListAanvragen,detailaanvraag,saveAanvraag,deleteAanvraag,detail,edit,delete,email,sendEmail,export,contactInfoReg,contactInfoSave,deleteArchive']
    );
	ExtensionUtility::configurePlugin(
        'Churchpersreg',
        'UpdatelogSearch',
		[PersoonController::Class => 'searchUpdatelog,selectedListUpdatelogs,detailUpdatelog'],
		[PersoonController::Class => 'searchUpdatelog,selectedListUpdatelogs,detailUpdatelog']
    );


	$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['cleanteamtoekenning'] = \Parousia\Churchpersreg\Hooks\cleanteamtoekenning::class .'::processRequest';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['selectperson'] = \Parousia\Churchpersreg\Hooks\selectperson::class .'::processRequest';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['zoekpostcode'] = \Parousia\Churchpersreg\Hooks\zoekpostcode::class .'::processRequest';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['filehandling'] = \Parousia\Churchpersreg\Hooks\filehandling::class .'::processRequest';
	$GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][701] = 'EXT:churchpersreg/Resources/Private/Templates/Email';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['changepersbyowner'] = \Parousia\Churchpersreg\Hooks\changepersbyowner::class .'::processRequest';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['toonfoto'] = \Parousia\Churchpersreg\Hooks\toonfoto::class .'::processRequest';
};

$boot();
unset($boot);